*Copyright (c) 2017-2022 Johan Berglund*  
*MotionParadigm is distributed under the terms of the GNU General Public License*

*This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.*

*This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.*

*You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.*


ABOUT
-----
MotionParadigm is a Python script to generate movie files, intended to guide head motion in controlled motion experiments. 
The movies are configured by human readable .json files which are input to the script. 
The output can be .gif or .mp4 files, which are made using ffmpeg. 

HOW TO USE
----------
`Example: python MotionParadigm.py -c "yaw.json"`

The -c flag specifies which configuration file to use as input.  

DEPENDENCIES
-------------------------------------------------------------------------------
See [./environment.yml](environment.yml).

CONTACT INFORMATION
-------------------
Johan Berglund, Ph.D.  
Uppsala University Hospital,  
Uppsala, Sweden  
johan.berglund@akademiska.se