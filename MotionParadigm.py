#!/usr/bin/env python3

import argparse
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
import json
import time
import FFMPEGwriter
import re
import datetime


def pointShape(x, y, X, Y):
    a = 1.0
    b = .2
    return 1-(a*(X-x)**2 + a*(Y-y)**2)**b


def plotPose(Rx, Ry, Rz, X, Y, ax, scale, align='center'):
    ax.clear()
    plt.axis((-16/9,16/9,-1,1))
    plt.axis('off')

    x, y = Rz * scale, Rx * scale

    Z = pointShape(x, y, X, Y)
    ax.imshow(Z, interpolation='bilinear', cmap='viridis',
                origin='lower', extent=[-16/9, 16/9, -1, 1],
                vmax=1, vmin=0)
    
    if 'left' in align:
        x -= 7/9
    elif 'right' in align:
        x += 7/9

    # Plot crosshair
    lw = .1
    r1, r2 = .05, .2
    for theta in [0, 90, 180, 270]:
        ax.plot([x + r1 * np.cos(np.radians(Ry + theta)), x + r2 * np.cos(np.radians(Ry + theta))], 
                [y + r1 * np.sin(np.radians(Ry + theta)), y + r2 * np.sin(np.radians(Ry + theta))], '-k', linewidth=lw)
    
    ax.add_patch(Circle((x, y), .15, fill=False, lw=lw, fc='black'))
    ax.add_patch(Circle((x, y), .1, fill=False, lw=lw, fc='black'))


def initFig(dpi, align):
    fig = plt.figure(frameon=False, figsize=(16/9, 1), dpi=dpi)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('off')
    if 'left' in align:
        xr = np.arange(-1, 23/9, 0.01)
    elif 'center' in align:
        xr = np.arange(-16/9, 16/9, 0.01)
    elif 'right' in align:
        xr = np.arange(-23/9, 1, 0.01)
    yr = np.arange(-1, 1, 0.01)
    X, Y = np.meshgrid(xr, yr)
    return fig, ax, X, Y


def makeMovie(rotations, config):
    fig, ax, X, Y = initFig(config['dpi']['value'], config['align']['value'])
    
    ffmpegWriter = FFMPEGwriter.FFMPEGwriter(config['fps']['value'])
    
    # Plot frames and write to the pipe
    print('Writing {} frames to {}'.format(len(rotations), config['outFile']['value']))
    for i, (Rx, Ry, Rz) in enumerate(rotations):
        plotPose(Rx, Ry, Rz, X, Y, ax, config['scale']['value'], config['align']['value'])
        plt.draw()
        ffmpegWriter.addFrame(fig)
    plt.close()
    ffmpegWriter.write(config['outFile']['value'])


def centerMotion(rotations):
    for dim in range(3):
        rotations[:, dim] -= np.average(rotations[:, dim])
    return rotations


def getMotionFromFile(config):
    file = config['inFile']['value']
    timeRegExp = '\d{2}:\d{2}:\d{2}.\d{3}'
    floatRegExp = '-?[0-9]+\.[0-9]+?'
    estimateRegExp = '({})\s+({})\s+({})\s+({})\s+({})\s+({})\s+({})\s+'.format(timeRegExp, floatRegExp, floatRegExp, floatRegExp, floatRegExp, floatRegExp, floatRegExp)

    lines = open(file, 'r').readlines()
    start = stop = None
    estimatedRotations = []
    t = []
    for line in lines:
        match = re.search(estimateRegExp, line)
        if (match):
            time = datetime.datetime.strptime(match.groups()[0], '%H:%M:%S.%f')
            if not start:
                start = time
            stop = time
            t.append((time-start).total_seconds())
            estimatedRotations.append([-float(match.groups()[4]), 
                                       -float(match.groups()[5]), 
                                       -float(match.groups()[6])])
    estimatedRotations = np.array(estimatedRotations)
    tFrames = np.arange(t[0], t[-1], 1/config['fps']['value'])
    rotations = np.zeros((len(tFrames), 3))
    for dim in range(3):
        rotations[:, dim] = np.interp(tFrames, t, estimatedRotations[:, dim])
    
    return centerMotion(rotations)


def getMotion(config):
    if 'inFile' in config:
        return getMotionFromFile(config)
    elif 'poseDuration' and 'moveDuration' in config and config['poseDuration']['value']>0: # stepwise motion
        N = int(np.ceil(config['duration']['value']/(config['poseDuration']['value']+config['moveDuration']['value'])))
        if 'circular' in config['type']['value']:    
            poses = np.array([(np.cos(2*np.pi*i/N), 0 , np.sin(2*np.pi*i/N)) for i in range(N)]) * config['range']['value']/2
        elif config['type']['value'] in ['yaw', 'pitch', 'roll']:
            pose = np.array([(2*i-N+1)/(N-1) for i in range(N)]) * config['range']['value']/2
            if 'pitch' in config['type']['value']:
                poses = [(c, 0, 0) for c in pose]
            elif 'roll' in config['type']['value']:
                poses = [(0, c, 0) for c in pose]
            elif 'yaw' in config['type']['value']:
                poses = [(0, 0, c) for c in pose]
        elif 'cocktail' in config['type']['value']:
            # First go through these discrete poses:
            poses = np.array([(0, 0, 0), (-1, 0, 0), (1, 0, 0), (0, 0, 0), (0, -1, 0), (0, 1, 0), (0, 0, 0), (0, 0, -1), (0, 0, 1)]) * config['range']['value']/2
            
        elif 'random' in config['type']['value']:
            if not 'moveProbability' in config:
                raise Exception('Random motion requires "moveProbability"')
            poses = [(0.0, 0.0, 0.0)]
            while len(poses)<N:
                if np.random.random()>config['moveProbability']['value']:
                    poses.append(poses[-1]) # no move
                else:
                    poses.append((np.random.random()*2-1, 0, np.random.random()*2-1)) # random move
            poses = np.array(poses) * config['range']['value']/2
        else:
            raise Exception('Stepwise motion "{}" not supported'.format(config['type']['value']))    
        
        nPoseFrames = round(config['fps']['value']*config['poseDuration']['value'])
        nMoveFrames = round(config['fps']['value']*(config['moveDuration']['value']+config['poseDuration']['value']))-nPoseFrames
        rotations = []
        for p, pose in enumerate(poses):
            for _ in range(nPoseFrames):
                rotations.append(pose)
            if p<len(poses)-1:
                dRz = poses[p+1][0]-pose[0]
                dRy = poses[p+1][1]-pose[1]
                dRx = poses[p+1][2]-pose[2]
                for i in range(nMoveFrames):
                    rotations.append(tuple([pose[0]+i*dRz/nMoveFrames, pose[1]+i*dRy/nMoveFrames, pose[2]+i*dRx/nMoveFrames]))
        if 'cocktail' in config['type']['value']:
            # ...then make a continuous circle:
            N = int(config['fps']['value'] * config['duration']['value'])
            for n in range(N):
                rotations.append(np.array((np.sin(2*np.pi*n/N), 0, np.cos(2*np.pi*n/N))) * config['range']['value']/2)
    else: # continuous motion
        N = int(config['fps']['value']*config['duration']['value'])
        if 'circular' in config['type']['value']:
            rotations = np.array([(np.sin(2*np.pi*i/N), 0, np.cos(2*np.pi*i/N)) for i in range(N)]) * config['range']['value']/2 
        elif config['type']['value'] in ['yaw', 'pitch', 'roll']:
            trajectory = np.array([np.sin(2*np.pi*i/N) for i in range(N)]) * config['range']['value']/2 
            trajectory = [trajectory[i]*np.sin(2*np.pi*i/N) for i in range(int(N/4))] \
                    + [trajectory[i] for i in range(int(N/4), int(N*3/4))] \
                    + [-trajectory[i]*np.sin(2*np.pi*i/N) for i in range(int(N*3/4), N)]
            if 'pitch' in config['type']['value']:
                rotations = [(c, 0, 0) for c in trajectory]
            elif 'roll' in config['type']['value']:
                rotations = [(0, c, 0) for c in trajectory]
            elif 'yaw' in config['type']['value']:
                rotations = [(0, 0, c) for c in trajectory]
        else: 
            raise Exception('Continuous motion "{}" not supported'.format(config['type']['value']))
    return rotations


def run(configFile):
    with open(configFile, 'r') as f:
        config = json.load(f)

    # Get motion in terms of head rotations [degrees]
    rotations = getMotion(config)

    # Make the movie
    t = time.time()
    makeMovie(rotations, config)
    print('Wrote {} frames of {} dpi in {:.1f} seconds'.format(len(rotations), config['dpi']['value'], time.time()-t))


def parseAndRun():
    ''' Command line parser. Parse command line and run main program. '''

    # Initiate command line parser
    parser = argparse.ArgumentParser(description='Create movie file to guide head motion for MRI motion correction')
    parser.add_argument('--configFile', '-c',
                        help="Name of configuration text file (.json)",
                        type=str,
                        default='circular.json')

    # Parse command line
    args = parser.parse_args()

    # Run main program
    run(args.configFile)


if __name__ == '__main__':
    parseAndRun()